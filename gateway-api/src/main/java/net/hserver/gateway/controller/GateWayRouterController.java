package net.hserver.gateway.controller;

import net.hserver.gateway.dto.GatewayRouterNodeDTO;
import net.hserver.gateway.service.ServerService;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.ioc.annotation.POST;
import top.hserver.core.server.util.JsonResult;
import net.hserver.gateway.dto.GatewayRouterDTO;
import net.hserver.gateway.service.GatewayRouterService;

/**
 * 服务节点管理
 *
 * @author hxm
 */
@Controller(value = "/v1/router/", name = "路由管理 ")
public class GateWayRouterController {

    @Autowired
    private GatewayRouterService gatewayRouterService;

    @Autowired
    private ServerService serverService;

    @POST("add")
    public JsonResult add(GatewayRouterDTO gatewayRouterDTO) {
        String add = gatewayRouterService.add(gatewayRouterDTO);
        if (add != null) {
            serverService.addOrUpdateServerData(gatewayRouterService.getOne(add));
            return JsonResult.ok();
        } else {
            return JsonResult.error();
        }
    }

    @POST("update")
    public JsonResult update(GatewayRouterDTO gatewayRouterDTO) {
        if (gatewayRouterService.update(gatewayRouterDTO)) {
            //刷新网关列表
            serverService.addOrUpdateServerData(gatewayRouterService.getOne(gatewayRouterDTO.getId()));
            return JsonResult.ok();
        } else {
            return JsonResult.error();
        }
    }


    @GET("list")
    public JsonResult list(Long page, Long pageSize) {
        return JsonResult.ok().put("data", gatewayRouterService.list(page, pageSize));
    }

    @GET("remove")
    public JsonResult remove(String id) {
        if (gatewayRouterService.remove(id)) {
            //刷新网关列表
            serverService.removeServerData(id);
            return JsonResult.ok();
        } else {
            return JsonResult.error();
        }
    }
}
