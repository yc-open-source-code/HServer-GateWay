package net.hserver.gateway.controller;

import net.hserver.gateway.service.ServerService;
import org.beetl.sql.core.query.Query;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.server.util.JsonResult;
import net.hserver.gateway.core.statistics.StatisticsData;
import net.hserver.gateway.dao.GatewayStatisticsDao;
import net.hserver.gateway.entity.GateWayStatistics;
import net.hserver.gateway.utils.JvmStackUtil;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

/**
 * @author hxm
 */
@Controller(value = "/v1/server/", name = "服务管理 ")
public class ServerController {

    @Autowired
    private ServerService serverService;

    @Autowired
    private GatewayStatisticsDao statisticsDao;

    @GET("restart")
    public JsonResult restart() {
        serverService.restartServerData();
        return JsonResult.ok();
    }

    @GET("nowData")
    public JsonResult nowData() {
        //获取最近一周的数据
        LocalDate now = LocalDate.now();
        LocalDate localDate = now.plusDays(-8);
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        List<GateWayStatistics> data = statisticsDao.createQuery().orEq("create_time", now.format(fmt)).select();
        List<GateWayStatistics> data2 = statisticsDao.createQuery().andBetween("create_time",localDate.format(fmt),now.format(fmt)).select();
        return JsonResult.ok().put("data", data.get(0)).put("mem", JvmStackUtil.getMemoryInfo()).put("week", data2);
    }

}
