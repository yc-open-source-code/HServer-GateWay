package net.hserver.gateway.service;

import net.hserver.gateway.dto.GatewayRouterNodeDTO;

import java.util.List;

/**
 * @author hxm
 */
public interface ServerService {

    /**
     * 启动初始化
     */
    void initServerData();

    /**
     * 添加或者更新
     *
     * @param gatewayRouterNodeDTO
     */
    void addOrUpdateServerData(List<GatewayRouterNodeDTO> gatewayRouterNodeDTO);

    /**
     * 移除
     *
     * @param routerId
     */
    void removeServerData(String routerId);

    /**
     * 全部重新来
     */
    void restartServerData();

}
