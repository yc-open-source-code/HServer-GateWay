package net.hserver.gateway.service;

import org.beetl.sql.core.page.PageResult;
import net.hserver.gateway.entity.GateWayNode;

import java.util.List;

/**
 * @author hxm
 */
public interface GatewayNodeService {

    /**
     * 添加服务节点
     * @param gateWayNode
     * @return
     */
    boolean add(GateWayNode gateWayNode);

    boolean update(GateWayNode gateWayNode);

    boolean remove(String id);

    PageResult<GateWayNode> list(Long page, Long pageSize);

    List<GateWayNode> all();

}
