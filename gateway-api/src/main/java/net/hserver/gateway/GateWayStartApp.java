package net.hserver.gateway;

import top.hserver.HServerApplication;
import top.hserver.core.ioc.annotation.HServerBoot;

/**
 * @author hxm
 */
@HServerBoot
public class GateWayStartApp {
    public static void main(String[] args) {
        HServerApplication.run(GateWayStartApp.class,9999, args);
    }
}
