package net.hserver.gateway.init;

import net.hserver.gateway.core.HServerGateWay;
import net.hserver.gateway.service.ServerService;
import top.hserver.core.interfaces.InitRunner;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Bean;

/**
 * @author hxm
 */
@Bean
public class InitGateWay implements InitRunner {


    @Autowired
    private ServerService serverService;

    @Override
    public void init(String[] args) {
        try {
            //初始化节点
            serverService.initServerData();
            HServerGateWay hServerGateWay = new HServerGateWay();
            hServerGateWay.setName("GateWay");
            hServerGateWay.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
