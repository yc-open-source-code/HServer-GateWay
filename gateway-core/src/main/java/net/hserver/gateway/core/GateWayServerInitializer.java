package net.hserver.gateway.core;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;

/**
 * @author hxm
 */
public class GateWayServerInitializer extends ChannelInitializer<Channel> {

  @Override
  protected void initChannel(Channel channel) throws Exception {
    ChannelPipeline ch = channel.pipeline();
    ch.addLast(new HttpServerCodec());
    ch.addLast(new HttpObjectAggregator(Integer.MAX_VALUE));
    ch.addLast(new FrontendHandler());
  }
}
