package net.hserver.gateway.core.strategy;


import net.hserver.gateway.core.strategy.imp.*;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Bean;
import net.hserver.gateway.core.node.Manager;

/**
 * 均衡负载策略
 *
 * @author hxm
 */
@Bean
public class LoadBalancingStrategy implements Strategy<ServerNode> {

    @Autowired
    private Manager manager;

    @Override
    public ServerNode polling(String routerId) {
        Chose<ServerNode> product = StrategyFactory.getProduct(manager.getHealthy(routerId), PollingStrategy.class);
        return product.choseInfo(null);
    }

    @Override
    public ServerNode random(String routerId) {
        Chose<ServerNode> product = StrategyFactory.getProduct(manager.getHealthy(routerId), RandomStrategy.class);
        return product.choseInfo(null);
    }

    @Override
    public ServerNode hashConsistency(String routerId,String ip) {
        Chose<ServerNode> product = StrategyFactory.getProduct(manager.getHealthy(routerId), HashConsistencyStrategy.class);
        return product.choseInfo(null);
    }

    @Override
    public ServerNode weightPolling(String routerId) {
        Chose<ServerNode> product = StrategyFactory.getProduct(manager.getHealthy(routerId), WeightPollingStrategy.class);
        return product.choseInfo(null);
    }

    @Override
    public ServerNode minimumConnection(String routerId) {
        Chose<ServerNode> product = StrategyFactory.getProduct(manager.getHealthy(routerId), MinimumConnectionStrategy.class);
        return product.choseInfo(null);
    }

    /**
     * 测试
     *
     * @param args
     */
    public static void main(String[] args) {
        Strategy strategy = new LoadBalancingStrategy();
        for (int i = 0; i < 10; i++) {
//            System.out.println(strategy.random());
        }

    }

}
