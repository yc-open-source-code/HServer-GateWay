package net.hserver.gateway.core.strategy;


import jdk.nashorn.internal.ir.IfNode;
import lombok.Data;
import net.hserver.gateway.config.StrategyType;

import java.net.InetSocketAddress;

/**
 * 服务节点
 *
 * @author hxm
 */
@Data
public class ServerNode {


    public ServerNode() {
    }

    /**
     * 上线，还是备份的用的，true 是备用的
     */
    private Boolean type;

    /**
     * 节点属性
     */
    private InetSocketAddress inetSocketAddress;

    /**
     * 节点Id
     */
    private String nodeId;

    /**
     * 节点描述名字
     */
    private String nodeDesc;

    /**
     * 节点组Id
     */
    private String routerId;

    /**
     * 组名
     */
    private String routerDesc;

    /**
     * url
     */
    private String url;

    /**
     * 是否是健康的
     */
    private Boolean health;

    /**
     * 连接数
     */
    private long connectNum;

    /**
     * 重试错误数
     */
    private int errorNum;

    /**
     * 权重
     */
    private int weight;

    /**
     * 当前权重
     */
    private int currentWeight;

    /**
     * 负载算法
     */
    private StrategyType strategyType;

    /**
     * Server哈希计算
     */
    private Integer hashCode;

    /**
     * hash
     * @return
     */
    public int getHashCode() {
        if (hashCode == null) {
            hashCode = inetSocketAddress.toString().hashCode();
        }
        return hashCode;
    }

}
