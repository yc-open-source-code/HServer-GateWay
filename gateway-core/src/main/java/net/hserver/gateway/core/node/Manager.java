package net.hserver.gateway.core.node;

import net.hserver.gateway.core.bean.RouterInfo;
import net.hserver.gateway.core.strategy.ServerNode;

import java.util.List;

/**
 * @author hxm
 */
public interface Manager {

    /**
     * 获取所有的服务节点
     *
     * @return
     */
    List<ServerNode> getAllServerNode();

  /**
   * 获取一个节点
   * @param routerId
   * @return
   */
    boolean isExistServerNode(String routerId);

    /**
     * 标记服务不健康
     *
     * @param routerId
     * @param nodeId
     * @return
     */
    Boolean markUnhealthy(String routerId, String nodeId);


    /**
     * 添加服务节点
     *
     * @param routerId
     * @param serverNodes
     */
    void addServerNode(String routerId, ServerNode serverNodes);

    /**
     * 移除路由
     *
     * @param routerId
     */
    void removeServerNode(String routerId);

    /**
     * 连接数叠加
     *
     * @param routerId
     * @param nodeId
     */
    void addConnect(String routerId, String nodeId);

    /**
     * 获取健康节点
     *
     * @return
     */
    List<ServerNode> getHealthy(String routerId);

    /**
     * 通过URL 匹配最佳的路由ID
     *
     * @param url
     * @return
     */
    RouterInfo getRouterIdByUrl(String url);

    /**
     * 清除所有
     */
    void clearAll();


}
