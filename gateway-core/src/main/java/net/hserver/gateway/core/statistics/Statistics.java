package net.hserver.gateway.core.statistics;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequest;

/**
 * @author hxm
 */
public interface Statistics {

  void uv(ChannelHandlerContext ctx);

  void pv();

  void platform(HttpRequest httpRequest);

}
