package net.hserver.gateway.core.statistics;


import lombok.Builder;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author hxm
 */
public class StatisticsData {

  private final static ConcurrentSkipListSet<String> uvData=new ConcurrentSkipListSet<>();
  private final static AtomicLong uv=new AtomicLong(0);
  private final static AtomicLong pv=new AtomicLong(0);
  private final static AtomicLong pcPlatform=new AtomicLong(0);
  private final static AtomicLong androidPlatform=new AtomicLong(0);
  private final static AtomicLong iosPlatform=new AtomicLong(0);
  private final static AtomicLong otherPlatform=new AtomicLong(0);


  public static void addUv(String ip){
    if (uvData.contains(ip)){
      return;
    }
    uvData.add(ip);
    uv.incrementAndGet();
  }

  public static void addPv(){
    pv.incrementAndGet();
  }

  public static void addPcPlatform(){
    pcPlatform.incrementAndGet();
  }

  public static void addAndroidPlatform(){
    androidPlatform.incrementAndGet();
  }

  public static void addIosPlatform(){
    iosPlatform.incrementAndGet();
  }

  public static void addOtherPlatform(){
    otherPlatform.incrementAndGet();
  }

  /**
   * 获取数据
   * @return
   */
  public static Data getData(){
    return Data.builder()
            .uv(uv.get())
            .pv(pv.get())
            .pcPlatform(pcPlatform.get())
            .androidPlatform(androidPlatform.get())
            .iosPlatform(iosPlatform.get())
            .otherPlatform(otherPlatform.get())
            .build();
  }

  /**
   * 获取并清空
   * @return
   */
  public static Data clearAndGet(){
    uvData.clear();
    return Data.builder()
      .uv(uv.getAndSet(0))
      .pv(pv.getAndSet(0))
      .pcPlatform(pcPlatform.getAndSet(0))
      .androidPlatform(androidPlatform.getAndSet(0))
      .iosPlatform(iosPlatform.getAndSet(0))
      .otherPlatform(otherPlatform.getAndSet(0))
      .build();
  }

  @lombok.Data
  @Builder
  public static class Data{
    private Long uv;
    private Long pv;
    private Long pcPlatform;
    private Long androidPlatform;
    private Long iosPlatform;
    private Long otherPlatform;
  }

}
