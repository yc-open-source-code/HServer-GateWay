package net.hserver.gateway.core.strategy;

public interface Strategy<T> {

  /**
   * 1、轮询法
   * @return
   */
  T polling(String routerId);

  /**
   * 2、随机法
   * @return
   */
  T random(String routerId);

  /**
   * 3、源地址哈希法(哈希一致性)
   * @return
   */
  T hashConsistency(String routerId,String ip);

  /**
   * 4、加权轮询法
   * @return
   */
  T weightPolling(String routerId);

  /**
   * 5、最小连接数法
   * @return
   */
  T minimumConnection(String routerId);

}
