package net.hserver.gateway.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

/**
 * @author hxm
 */
@Data
@Table(name = "gateway_statistics")
public class GateWayStatistics {

  @AssignID
  private String id;

  private String uv;

  private String pv;

  private String pcPlatform;

  private String androidPlatform;

  private String iosPlatform;

  private String otherPlatform;

  private String createTime;

}
