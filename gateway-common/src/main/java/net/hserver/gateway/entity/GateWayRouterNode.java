package net.hserver.gateway.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

/**
 * @author hxm
 */
@Table(name = "gateway_router_node")
@Data
public class GateWayRouterNode {
    @AssignID
    private String id;
    private String routerId;
    private String nodeId;
}
