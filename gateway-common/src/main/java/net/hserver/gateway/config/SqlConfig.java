package net.hserver.gateway.config;

import com.zaxxer.hikari.HikariDataSource;
import net.hserver.gateway.utils.FileUtil;
import org.beetl.sql.core.*;
import org.beetl.sql.core.db.DBStyle;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.core.db.SQLiteStyle;
import org.beetl.sql.core.loader.MarkdownClasspathLoader;
import org.beetl.sql.ext.DBInitHelper;
import org.beetl.sql.ext.DebugInterceptor;
import org.beetl.sql.mapper.annotation.Param;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.ioc.annotation.Configuration;
import top.hserver.core.ioc.annotation.Value;

import java.io.File;
import java.io.InputStream;

/**
 * @author hxm
 */
@Configuration
public class SqlConfig {
    @Bean
    public SQLManager sql() {
        try {

            String path = System.getProperty("user.dir") + File.separator + "db.db";
            File file = new File(path);
            boolean flag = false;
            if (!file.exists()) {
                //进行导出初始化一个sqlite
                InputStream is = SqlConfig.class.getResourceAsStream("/db/db.db");
                FileUtil.copyFile(is, path);
                flag = true;
            }
            HikariDataSource ds = new HikariDataSource();
            ds.setJdbcUrl("jdbc:sqlite:" + path);
            ds.setDriverClassName("org.sqlite.JDBC");
            ConnectionSource source = ConnectionSourceHelper.getSingle(ds);
            SQLManagerBuilder builder = new SQLManagerBuilder(source);
            builder.setSqlLoader(new MarkdownClasspathLoader());
            builder.setNc(new UnderlinedNameConversion());
            builder.setInters(new Interceptor[]{new DebugInterceptor()});
            builder.setDbStyle(new SQLiteStyle());
            SQLManager sqlManager = builder.build();
            if (flag) {
                try {
                    DBInitHelper.executeSqlScript(sqlManager, "db/gateway.sql");
                } catch (Exception e) {
                }
            }
            return sqlManager;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
